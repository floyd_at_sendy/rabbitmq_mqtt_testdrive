package app;

import org.eclipse.paho.client.mqttv3.MqttCallback;

public class Sample implements MqttCallback {
    /**
     * The main entry point of the sample
     *
     * This method handles parsing of the arguments specified
     * on the command-line before performing the specified action.
     */
    public static void main(String[] args) {
        // Default settings:
        boolean quiteMode = false;
        String action = "publish";
        String topic = "";
        String message = "Message from blocking Paho MQTTv3 client sample";
        int qos = 2;
        String broker = "localhost";
        int port = 1883;
        String clientId = null;
        String subTopic = "Sample/#";
        String pubTopic = "Sample/Java/v3";
        boolean cleanSession = true;    // Non durable subscriptions
        boolean ssl = false;
        String password = null;
        String userName = null;

        // Parse the arguments
        for (int i = 0; i < args.length; i++) {
            // Check this is a valid argument
            if (args[i].length() == 2 && args[i].startsWith("-")) {
                char arg = args[i].charAt(1);
                // Handle arguments that take no-value
                switch(arg) {
                    case 'h' : case '?': printHelp(); return;
                    case 'q': quiteMode = true; continue;
                }

                // Now handle the arguments that take a value and ensure one is
                // specified.
                if (i == args.length - 1 || args[i + 1].charAt(0) == '-') {
                    System.out.println("Missing value for argument: "+ args[i]);
                    printHelp();
                    return;
                }

                switch(arg) {
                    case 'a': action = args[++i]; break;
                    case 't': topic = args[++i]; break;
                    case 'm': message = args[++i]; break;
                    case 's': qos = Integer.parseInt(args[++i]); break;
                    case 'b': broker = args[++i];
                    case 'p': port = Integer.parseInt(args[++i]); break;
                    case 'i': clientId = args[++i]; break;
                    case 'c': cleanSession = Boolean.valueOf(args[++i]); break;
                    case 'k': System.getProperties().put("javax.net.ssl.keyStore", args[++i]); break;
                    case 'w': System.getProperties().put("javax.net.ssl.keyStorePassword", args[++i]); break;
                    case 'r': System.getProperties().put("javax.net.ssl.trustStore", args[++i]); break;
                    case 'v': ssl = Boolean.valueOf(args[++i]); break;
                    case 'u': userName = args[++i]; break;
                    case 'z': password = args[++i]; break;
                    default:
                        System.out.println("Unrecognised argument: " + args[i]);
                        printHelp();
                        return;
                }
            }
        }
    }
}
